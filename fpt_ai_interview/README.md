# Bài tập phỏng vấn vị trí NLP Research Engineer

Tác giả: Phạm Quang Nhật Minh (ĐH FPT)

Yêu cầu của bài tập này là xây dựng một bộ phân lớp văn bản từ một tập dữ liệu
cho trước, bao gồm các bài báo crawl từ báo vnexpress.

## 1. Chuẩn bị dữ liệu

Download file ```vnexpress_data.csv.tgz``` trong thư mục ```data```. Giải nén file.

File dữ liệu tuân theo định dạng csv bao gồm 3 trường: category, title, content. Trong đó:

- category là category (lớp) của bài báo
- title là tiêu đề của bài báo
- content là nội dung của bài báo

## 2. Thống kê phân bố các lớp trên dữ liệu

Thống kê số lượng các samples trong mỗi lớp trong dữ liệu đầu vào theo 2 cách khác nhau:

- Viết 1 python script
- Chỉ sử dụng các Linux command lines

## 3. Tiền xử lý dữ liệu

Trong dữ liệu đầu vào còn có khá nhiều nhiễu (như các HTML tags, kí tự đặc biệt,...). 
Dữ liệu cũng chưa được tách từ (word segmentation). Yêu cầu của bài tập này là tiền xử lý liệu và 
lưu vào một file output với định dạng giống như file ban đầu. Viết 1 script duy nhất để thực hiện việc này. Việc 
thực thi chương trình tiền xử lý theo syntax sau (tên script có thể thay đổi):

```
./preprocess input_file output_file
```

Chương trình tiền xử lý dữ liệu thực hiện các việc sau đây (yêu cầu tối thiểu) cho cả title và content của bài báo:

- Loại bỏ các HTML tags
- Loại bỏ tên tác giả cuối mỗi bài báo
- Tách từ
- Loại bỏ các dấu câu và kí tự đặc biệt

Gợi ý: Có thể xử dụng bộ tách từ của tác giả Lê Hồng Phương tại [đây](https://github.com/phuonglh/vn.vitk).

## 4. Phân chia dữ liệu thành dữ liệu train và test

Viết chương trình phân chia tập dữ liệu đã qua tiền xử lý thành 2 tập ```train.txt``` và ```test.txt``` theo tỉ lệ 90% cho training và 10% cho test. Chương trình cần phân chia sao cho phân bố của các nhãn là giống nhau cho tập train và tập test.

## 5. Xây dựng bộ phân lớp

Viết chương trình huấn luyện bộ phân lớp bằng các thuật toán Support Vector Machines (SVM) và Naive Bayes trên dữ liệu huấn luyện (```train.txt```). Sau đó báo cáo kết quả phân lớp trên tập dữ liệu test (```test.txt```). Ở đây, chúng ta dùng độ đo là độ chính xác (accuracy) và F1-score (báo cáo F1-score cho từng lớp và trung bình cho các lớp). So sánh kết quả của 2 thuật toán.

Trong bài tập này, chúng ta sử dụng features đơn giản là word identity. Tức là chúng ta sẽ xác định xem một word có xuất hiện trong văn bản hay không.

Bạn có thể chỉ sử dụng title, content hoặc title+content của bài báo cho việc xây dựng mô hình phân lớp.

## 6. Chọn tham số phù hợp dùng grid search và k-fold cross validation

Chọn các tham số tốt nhất cho mô hình phân lớp SVM với grid search và k-fold cross validation trên tập dữ liệu huấn luyện. Báo cáo kết quả dự đoán của mô hình (với các tham số thu được) trên tập test.

## 7. Thử nghiệm công cụ XGBoost

Thử nghiệm công cụ [XGBoost](https://xgboost.readthedocs.io/en/latest) để xây dựng mô hình phân lớp với dữ liệu huấn luyện và báo cáo kết quả dự đoán trên tập test.

## 8. Đề xuất features hiệu quả

Dựa trên việc phân tích dữ liệu, đề xuất cách trích xuất đặc trưng hiệu quả cho dữ liệu đã cho.


